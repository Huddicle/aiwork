#ifndef H012164GTANK_H
#define H012164GTANK_H

#include "h012164gSteering.h"

//#include <SDL.h> has been moved to pathfinder class - I.H

//---------------------------------------------------------------
enum h012164gTANKSTATES
{
	ACCELERATING,
	BRAKING,
	HARDTURNING,
	STATIONARY,
	SOFTTURNING
};

class h012164gTank : public BaseTank
{
	//---------------------------------------------------------------
public:
	h012164gTank(SDL_Renderer* renderer, TankSetupDetails details);
	~h012164gTank();

	void ChangeState(BASE_TANK_STATE newState);

	void Update(float deltaTime, SDL_Event e);
	void Render();

	void SetCurrentMaxSpeed(float newMaxSpeed);

	//void h012164gRotateHeadingByRadian(double radian, int sign, float deltaTime);

	//---------------------------------------------------------------
protected:
	h012164gSteering steering;
	h012164gTANKSTATES currentState;

	unsigned char behaviours;

	Vector2D originalTankPosition;
	float currentMaxSpeed;

	bool collectingPickup;
	int prevPickupsSize;

	float runawayCooldownTimer;

	bool attackEnemy;

	bool	IsSameDirection(const Vector2D& vec1, const Vector2D& vec2);
	bool	IsSoftTurn(const Vector2D& vec1, const Vector2D& vec2);
	bool	IsHardTurn(const Vector2D& vec1, const Vector2D& vec2);
	void	MoveInHeadingDirection(float deltaTime);

	void h012164gRotateHeadingByRadian(float radian, int sign, float deltaTime);

	Vector2D targetPos;

private:
	TURN_DIRECTION  mTankTurnDirection;
	bool			mTankTurnKeyDown;
	MOVE_DIRECTION  mTankMoveDirection;
	bool			mTankMoveKeyDown;
	TURN_DIRECTION  mManTurnDirection;
	bool			mManKeyDown;
	bool			mFireKeyDown;

	void RotateHeadingByRadian(float radian, int sign);
	Vector2D currentNetForce;
};

//---------------------------------------------------------------

#endif //CONTROLLEDTANK_H