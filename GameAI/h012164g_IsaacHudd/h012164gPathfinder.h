#pragma once
#include <SDL.h>
#include "../BaseTank.h"
#include "../Commons.h"
#include "../TankManager.h"
#include "../ObstacleManager.h"
#include "../Collisions.h"
#include "../Waypoint.h"
#include "../WaypointManager.h"
#include "../PickUpManager.h"

struct h012164gAStarNode
{
	Waypoint* internalWaypoint;
	h012164gAStarNode* parentNode;
	float cost;

	h012164gAStarNode(Waypoint* currentWayPoint, h012164gAStarNode* newParentNode, float newCost) : internalWaypoint(currentWayPoint), parentNode(newParentNode), cost(newCost) {}
};

struct h012164gEdgeCost
{
	Waypoint* waypointFrom;
	Waypoint* waypointTo;
	float cost;

	h012164gEdgeCost(Waypoint* from, Waypoint* to) : waypointFrom(from), waypointTo(to) { cost = from->GetPosition().Distance(to->GetPosition()); }
};

class h012164gPathfinder
{
private:
	Waypoint* closestToMe;
	Waypoint* closestToTarget;
	bool reachedEndOfPath;

	vector<Waypoint*> _currentPath;

	vector<h012164gEdgeCost*>  _edgeCostList;
	vector<h012164gAStarNode*> _openList;
	vector<h012164gAStarNode*> _closedList;

	BaseTank* myTank;

	void SetEdgeCosts();
	vector<h012164gEdgeCost*> GetEdgesConnectedToNode(h012164gAStarNode* node);
	float GetHeuristicCost(const Vector2D& point1, const Vector2D& point2);
public:
	h012164gPathfinder(BaseTank* tank);
	~h012164gPathfinder();

	void CreatePathToPosition(Vector2D& myPosition, Vector2D& seekPosition);


	vector<Waypoint*> GetCurrentPath()									 { return _currentPath; }
	Waypoint* GetNextWaypoint(Vector2D& myPosition, Vector2D& seekPosition);

	void SetTank(BaseTank* newTank);

	void ResetPath();
};
