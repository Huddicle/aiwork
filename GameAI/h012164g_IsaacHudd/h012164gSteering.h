 #pragma once
#include "h012164gPathfinder.h"

enum h012164gBEHAVIOURS
{
	SEEK = 0x01,
	FLEE = 0x02,
	ARRIVE = 0x04,
	PURSUIT = 0x08,
	OBSTACLEAVOIDANCE = 0x10,
	//0x20
	//0x80
	//0x80
};

class h012164gSteering
{
private:
	float _seekWeight;
	float _fleeWeight;
	float _arriveWeight;
	float _pursuitWeight;
	float _avoidanceWeight;

	h012164gPathfinder* pathFinder;

	unsigned char behaviours;
	Vector2D* seekPosition;
	Vector2D currentTargetPosition;

	vector<Vector2D> _feelers;

	Vector2D _checkPointFront;
	Vector2D _checkPointFrontLeft;
	Vector2D _checkPointFrontRight;
	Vector2D _checkPointLeft;
	Vector2D _checkPointRight;
	Vector2D _checkPointLeftCorner;
	Vector2D _checkPointRightCorner;

	BaseTank* myTank;
	TankManager* tankManager;

	float preferredMaxSpeed;

	float pathRegenerationTimer;

	vector<BaseTank*>& tanksInSight;

	Vector2D Seek();
	Vector2D Flee();
	Vector2D Arrive();
	Vector2D Pursuit();
	Vector2D ObstacleAvoidance();

	
public:
	h012164gSteering(vector<BaseTank*>& sightList);
	~h012164gSteering();

	Vector2D GetNetForce(float t);
	float GetPreferredMaxSpeed() { return preferredMaxSpeed; }
	//void SetBehaviouralPattern();

	void SetSeekPosition(Vector2D* newPos);
	void SetMyTank(BaseTank* newTank);
	void SetBehaviours(unsigned char newBehaviours);

	vector<Vector2D> GetFeelers();
	h012164gPathfinder* GetPathfinder() { return pathFinder; }
};

