#include "h012164gSteering.h"

Vector2D h012164gSteering::Seek()
{
	Vector2D returnVector;

	returnVector.x = myTank->GetCentralPosition().x - currentTargetPosition.x;
	returnVector.y = myTank->GetCentralPosition().y - currentTargetPosition.y;

	returnVector.Normalize();

	return returnVector;

	/*Waypoint* targetWaypoint = pathFinder->GetNextWaypoint();
	returnVector.x = myTank->GetCentralPosition().x - targetWaypoint->GetPosition().x;
	returnVector.y = myTank->GetCentralPosition().y - targetWaypoint->GetPosition().y;*/
}

Vector2D h012164gSteering::Flee()
{
	Vector2D returnVector;

	returnVector.x = myTank->GetCentralPosition().x - currentTargetPosition.x;
	returnVector.y = myTank->GetCentralPosition().y - currentTargetPosition.y;

	returnVector.Normalize();

	return returnVector * -1;
}

Vector2D h012164gSteering::Arrive()
{
	Vector2D returnVector;

	returnVector.x = myTank->GetCentralPosition().x - currentTargetPosition.x;
	returnVector.y = myTank->GetCentralPosition().y - currentTargetPosition.y;

	returnVector.Normalize();

	return returnVector;
}

Vector2D h012164gSteering::Pursuit()
{
	if (tanksInSight.size() > 0)
	{
		currentTargetPosition.x = tanksInSight[0]->GetCentralPosition().x;
		currentTargetPosition.y = tanksInSight[0]->GetCentralPosition().y;
		currentTargetPosition.x += (tanksInSight[0]->GetHeading() * 75.0f).x;
		currentTargetPosition.y += (tanksInSight[0]->GetHeading() * 75.0f).y;
	}
	else
	{
		//seekPosition->x = myTank->GetCentralPosition().x;
		//seekPosition->y = myTank->GetCentralPosition().y;
	}

	return Seek();
}

Vector2D h012164gSteering::ObstacleAvoidance()
{
	vector<GameObject*> objects = ObstacleManager::Instance()->GetObstacles();

	Vector2D returnVector = Vector2D(0.0, 0.0);

	float speedMultiplier = myTank->GetVelocity().Length();
	_feelers.clear();
	_feelers.push_back(myTank->GetHeading() * speedMultiplier * 1.5f);
	_feelers.push_back(Vector2D(myTank->GetHeading().y * 20.0f, myTank->GetHeading().x * -20.0f) + (myTank->GetHeading() * speedMultiplier));
	_feelers.push_back(Vector2D(myTank->GetHeading().y * -20.0f, myTank->GetHeading().x * 20.0f) + (myTank->GetHeading() * speedMultiplier));
	_feelers.push_back(Vector2D(myTank->GetHeading().y * 25.0f, myTank->GetHeading().x * -25.0f));
	_feelers.push_back(Vector2D(myTank->GetHeading().y * -25.0f, myTank->GetHeading().x * 25.0f));
	_feelers.push_back(Vector2D(myTank->GetHeading().y * 25.0f, myTank->GetHeading().x * -25.0f) + (myTank->GetHeading() * 20.0f));
	_feelers.push_back(Vector2D(myTank->GetHeading().y * -25.0f, myTank->GetHeading().x * 25.0f) + (myTank->GetHeading() * 20.0f));

	_checkPointFront = (myTank->GetHeading() * speedMultiplier * 1.5f);
	_checkPointFrontLeft = (Vector2D(myTank->GetHeading().y * 20.0f, myTank->GetHeading().x * -20.0f) + (myTank->GetHeading() * speedMultiplier));
	_checkPointFrontRight = (Vector2D(myTank->GetHeading().y * -20.0f, myTank->GetHeading().x * 20.0f) + (myTank->GetHeading() * speedMultiplier));
	_checkPointLeft = (Vector2D(myTank->GetHeading().y * 25.0f, myTank->GetHeading().x * -25.0f));
	_checkPointRight = (Vector2D(myTank->GetHeading().y * -25.0f, myTank->GetHeading().x * 25.0f));
	_checkPointLeftCorner = (Vector2D(myTank->GetHeading().y * 25.0f, myTank->GetHeading().x * -25.0f) + (myTank->GetHeading() * 20.0f));
	_checkPointRightCorner = (Vector2D(myTank->GetHeading().y * -25.0f, myTank->GetHeading().x * 25.0f) + (myTank->GetHeading() * 20.0f));

	for (int i = 0; i < objects.size(); i++)
	{
		float distance = (myTank->GetCentralPosition() - objects[i]->GetCentralPosition()).Length();
		if (distance < 100.0f)
		{
			//preferredMaxSpeed = 50.0f;
		}

		vector<Vector2D> points = objects[i]->GetAdjustedBoundingBox();
		//cout << points[0].x << "," << points[0].y << "  " << points[1].x << "," << points[1].y << "  " << points[2].x << "," << points[2].y << "  " << points[3].x << "," << points[3].y << "  " << endl;
		
		float collisionSpeedDivisor = 5.0f;

		if (Collisions::Instance()->TriangleCollision(points[0], points[1], points[2], myTank->GetPointAtFrontOfTank() + _checkPointFront))
		{
			returnVector += _checkPointFront;
			preferredMaxSpeed /= collisionSpeedDivisor;
		}
		if (Collisions::Instance()->TriangleCollision(points[0], points[2], points[3], myTank->GetPointAtFrontOfTank() + _checkPointFront))
		{
			returnVector += _checkPointFront;
			preferredMaxSpeed /= collisionSpeedDivisor;
		}
		if (Collisions::Instance()->TriangleCollision(points[0], points[1], points[2], myTank->GetCentralPosition() + _checkPointFrontRight))
		{
			returnVector -= _checkPointFrontLeft;
			preferredMaxSpeed /= collisionSpeedDivisor;
		}
		if (Collisions::Instance()->TriangleCollision(points[0], points[2], points[3], myTank->GetCentralPosition() + _checkPointFrontRight))
		{
			returnVector -= _checkPointFrontLeft;
			preferredMaxSpeed /= collisionSpeedDivisor;
		}
		if (Collisions::Instance()->TriangleCollision(points[0], points[1], points[2], myTank->GetCentralPosition() + _checkPointFrontLeft))
		{
			returnVector -= _checkPointFrontRight;
			preferredMaxSpeed /= collisionSpeedDivisor;
		}
		if (Collisions::Instance()->TriangleCollision(points[0], points[2], points[3], myTank->GetCentralPosition() + _checkPointFrontLeft))
		{
			returnVector -= _checkPointFrontRight;
			preferredMaxSpeed /= collisionSpeedDivisor;
		}
		if (Collisions::Instance()->TriangleCollision(points[0], points[1], points[2], myTank->GetCentralPosition() + (_checkPointFrontRight / 2)))
		{
			returnVector -= _checkPointFrontLeft;
			preferredMaxSpeed /= collisionSpeedDivisor;
		}
		if (Collisions::Instance()->TriangleCollision(points[0], points[2], points[3], myTank->GetCentralPosition() + (_checkPointFrontRight / 2)))
		{
			returnVector -= _checkPointFrontLeft;
			preferredMaxSpeed /= collisionSpeedDivisor;
		}
		if (Collisions::Instance()->TriangleCollision(points[0], points[1], points[2], myTank->GetCentralPosition() + (_checkPointFrontLeft / 2)))
		{
			returnVector -= _checkPointFrontRight;
			preferredMaxSpeed /= collisionSpeedDivisor;
		}
		if (Collisions::Instance()->TriangleCollision(points[0], points[2], points[3], myTank->GetCentralPosition() + (_checkPointFrontLeft / 2)))
		{
			returnVector -= _checkPointFrontRight;
			preferredMaxSpeed /= collisionSpeedDivisor;
		}

		if (Collisions::Instance()->TriangleCollision(points[0], points[1], points[2], myTank->GetCentralPosition() + _checkPointRight))
		{
			returnVector -= _checkPointLeft;
			preferredMaxSpeed /= collisionSpeedDivisor;
		}
		if (Collisions::Instance()->TriangleCollision(points[0], points[2], points[3], myTank->GetCentralPosition() + _checkPointRight))
		{
			returnVector -= _checkPointLeft;
			preferredMaxSpeed /= collisionSpeedDivisor;
		}
		if (Collisions::Instance()->TriangleCollision(points[0], points[1], points[2], myTank->GetCentralPosition() + _checkPointLeft))
		{
			returnVector -= _checkPointRight;
			preferredMaxSpeed /= collisionSpeedDivisor;
		}
		if (Collisions::Instance()->TriangleCollision(points[0], points[2], points[3], myTank->GetCentralPosition() + _checkPointLeft))
		{
			returnVector -= _checkPointRight;
			preferredMaxSpeed /= collisionSpeedDivisor;
		}

		if (Collisions::Instance()->TriangleCollision(points[0], points[1], points[2], myTank->GetCentralPosition() + _checkPointLeftCorner))
		{
			returnVector -= _checkPointRightCorner;
			preferredMaxSpeed /= collisionSpeedDivisor;
		}
		if (Collisions::Instance()->TriangleCollision(points[0], points[2], points[3], myTank->GetCentralPosition() + _checkPointLeftCorner))
		{
			returnVector -= _checkPointRightCorner;
			preferredMaxSpeed /= collisionSpeedDivisor;
		}
		if (Collisions::Instance()->TriangleCollision(points[0], points[1], points[2], myTank->GetCentralPosition() + _checkPointRightCorner))
		{
			returnVector -= _checkPointLeftCorner;
			preferredMaxSpeed /= collisionSpeedDivisor;
		}
		if (Collisions::Instance()->TriangleCollision(points[0], points[2], points[3], myTank->GetCentralPosition() + _checkPointRightCorner))
		{
			returnVector -= _checkPointLeftCorner;
			preferredMaxSpeed /= collisionSpeedDivisor;
		}
	}

	if (returnVector.Length() > 0.0f)
	{
		returnVector.Normalize();
	}

	if (preferredMaxSpeed < 25.0f)
	{
		preferredMaxSpeed = 25.0f;
	}

	return returnVector;
}

h012164gSteering::h012164gSteering(vector<BaseTank*>& sightList) : tanksInSight(sightList), _seekWeight(50.0f), _fleeWeight(50.0f), _arriveWeight(50.0f), _pursuitWeight(50.0f), _avoidanceWeight(150.0f)
{
	pathFinder = new h012164gPathfinder(myTank);
	pathRegenerationTimer = 0.0f;
}


h012164gSteering::~h012164gSteering()
{
}

Vector2D h012164gSteering::GetNetForce(float t)
{
	Vector2D curForce = Vector2D(0.0, 0.0);
	Vector2D netForce = Vector2D(0.0, 0.0);
	preferredMaxSpeed = 75.0f;
	float netWeight = 0.0f;

	Vector2D seekForce = Vector2D(0.0, 0.0);
	Vector2D fleeForce = Vector2D(0.0, 0.0);
	Vector2D arriveForce = Vector2D(0.0, 0.0);
	Vector2D pursuitForce = Vector2D(0.0, 0.0);
	Vector2D avoidanceForce = Vector2D(0.0, 0.0);

	//pathFinder->CreatePathToPosition(myTank->GetCentralPosition(), *seekPosition);
	Waypoint* currentWaypoint = pathFinder->GetNextWaypoint(myTank->GetCentralPosition(), *seekPosition);
	if (currentWaypoint == nullptr)
	{
		currentTargetPosition = *seekPosition;
	}
	else
	{
		currentTargetPosition = currentWaypoint->GetPosition();
	}
	//cout << currentTargetPosition.x << " " << currentTargetPosition.y << endl;

	if (behaviours & h012164gBEHAVIOURS::SEEK)
	{
		seekForce += Seek();
		netWeight += _seekWeight;
	}

	if (behaviours & h012164gBEHAVIOURS::FLEE)
	{
		fleeForce += Flee();
		netWeight += _fleeWeight;
	}

	if (behaviours & h012164gBEHAVIOURS::ARRIVE)
	{
		arriveForce += Arrive();
		netWeight += _arriveWeight;
	}

	if (behaviours & h012164gBEHAVIOURS::PURSUIT)
	{
		pursuitForce += Pursuit();
		netWeight += _pursuitWeight;
	}

	if (behaviours & h012164gBEHAVIOURS::OBSTACLEAVOIDANCE)
	{
		avoidanceForce += ObstacleAvoidance();
		if (avoidanceForce.Length() > 0.0f)
		{
			netWeight += _avoidanceWeight;
		}
	}

	//float netMag = sqrt((netForce.x * netForce.x) + (netForce.y * netForce.y));

	//netForce /= netMag;
	float singleWeight = 1;// 75.0f / netWeight;

	if (behaviours & h012164gBEHAVIOURS::SEEK)
	{
		netForce += seekForce * singleWeight * _seekWeight;
	}

	if (behaviours & h012164gBEHAVIOURS::FLEE)
	{
		netForce += fleeForce * singleWeight * _fleeWeight;
	}

	if (behaviours & h012164gBEHAVIOURS::ARRIVE)
	{
		netForce += arriveForce * singleWeight * _arriveWeight;
	}

	if (behaviours & h012164gBEHAVIOURS::PURSUIT)
	{
		netForce += pursuitForce * singleWeight * _pursuitWeight;
	}

	if (behaviours & h012164gBEHAVIOURS::OBSTACLEAVOIDANCE)
	{
		if (avoidanceForce.Length() > 0.0f)
		{
			netForce += avoidanceForce * singleWeight * _avoidanceWeight;
		}
	}



	return netForce;
}

void h012164gSteering::SetSeekPosition(Vector2D * newPos)
{
	seekPosition = newPos;
}

void h012164gSteering::SetMyTank(BaseTank * newTank)
{
	myTank = newTank;
}

void h012164gSteering::SetBehaviours(unsigned char newBehaviours)
{
	behaviours = newBehaviours;
}

vector<Vector2D> h012164gSteering::GetFeelers()
{
	return _feelers;
}
