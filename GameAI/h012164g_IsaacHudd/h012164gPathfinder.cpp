#include "h012164gPathfinder.h"



vector<h012164gEdgeCost*> h012164gPathfinder::GetEdgesConnectedToNode(h012164gAStarNode * node)
{
	vector<h012164gEdgeCost*> edges = vector<h012164gEdgeCost*>();

	for (int i = 0; i < _edgeCostList.size(); i++)
	{
		if (_edgeCostList[i]->waypointFrom->GetID() == node->internalWaypoint->GetID())
		{
			edges.push_back(_edgeCostList[i]);
		}
	}
	return edges;
}

float h012164gPathfinder::GetHeuristicCost(const Vector2D & point1, const Vector2D & point2)
{
	return point1.Distance(point2);
}

h012164gPathfinder::h012164gPathfinder(BaseTank* tank) : myTank(tank)
{
	vector<Waypoint*> waypoints = WaypointManager::Instance()->GetAllWaypoints();
	for (int i = 0; i < waypoints.size(); i++)
	{
		//Set edge costs of all waypoints
		vector<int> connectedIDs = waypoints[i]->GetConnectedWaypointIDs();
		for (int j = 0; j < connectedIDs.size(); j++)
		{
			_edgeCostList.push_back(new h012164gEdgeCost(waypoints[i], WaypointManager::Instance()->GetWaypointWithID(connectedIDs[j])));
		}
	}
	reachedEndOfPath = false;
}


h012164gPathfinder::~h012164gPathfinder()
{
	
}

void h012164gPathfinder::CreatePathToPosition(Vector2D& myPosition, Vector2D& seekPosition)
{
	vector<Waypoint*> waypoints = WaypointManager::Instance()->GetAllWaypoints();
	_currentPath.clear();
	_openList.clear();
	_closedList.clear();

	return;


	closestToMe = waypoints[0];
	closestToTarget = waypoints[0];
	float closestWaypointDistanceToMe = (waypoints[0]->GetPosition() - myPosition).Length();
	float closestWaypointDistanceToTarget = (waypoints[0]->GetPosition() - seekPosition).Length();

	for (int i = 1; i < waypoints.size(); i++)
	{
		// Get the closest waypoint to the tank
		float tankToWaypointDistance = (waypoints[i]->GetPosition() - myPosition).Length();
		float targetToWaypointDistance = (waypoints[i]->GetPosition() - seekPosition).Length();
		if (tankToWaypointDistance < closestWaypointDistanceToMe)
		{
			closestToMe = waypoints[i];
			closestWaypointDistanceToMe = tankToWaypointDistance;
		}
		//Get the closest waypoint to the target
		if (targetToWaypointDistance < closestWaypointDistanceToTarget)
		{
			closestToTarget = waypoints[i];
			closestWaypointDistanceToTarget = targetToWaypointDistance;
		}

	}



	_openList.push_back(new h012164gAStarNode(closestToMe, NULL, 0.0f));

	h012164gAStarNode* currentNode = _openList[0];

	while (_openList.size() > 0)
	{
		for (int i = 0; i < _openList.size(); i++)
		{
			if (currentNode != NULL)
			{
				if (_openList[i]->cost < currentNode->cost)
				{
					currentNode = _openList[i];
				}
			}
			else
			{
				currentNode = _openList[i];
			}
		}


		if (currentNode->internalWaypoint == closestToTarget)
		{
			//Construct path and leave
			while (currentNode != NULL)
			{
				_currentPath.push_back(currentNode->internalWaypoint);
				currentNode = currentNode->parentNode;
			}
			return;
		}
		else
		{
			vector<h012164gEdgeCost*> connectedEdges = GetEdgesConnectedToNode(currentNode);
			for (int i = 0; i < connectedEdges.size(); i++)
			{
				bool addToOpenList = true;
				for (int j = 0; j < _openList.size(); j++)
				{
					if (connectedEdges[i]->waypointTo == _openList[j]->internalWaypoint)
					{
						addToOpenList = false;
					}
				}
				for (int j = 0; j < _closedList.size(); j++)
				{
					if (connectedEdges[i]->waypointTo == _closedList[j]->internalWaypoint)
					{
						addToOpenList = false;
					}
				}

				if (addToOpenList)
				{
					float g = currentNode->cost + connectedEdges[i]->cost;
					float h = GetHeuristicCost(currentNode->internalWaypoint->GetPosition(), seekPosition);
					float f = g + h;

					_openList.push_back(new h012164gAStarNode(connectedEdges[i]->waypointTo, currentNode, f));
				}
			}
			//Add to closed list
			_closedList.push_back(currentNode);
			//Remove from open list
			vector<h012164gAStarNode*>::iterator iter = _openList.begin();
			while (iter != _openList.end())
			{
				if (*iter == currentNode)
				{
					iter = _openList.erase(iter);
				}
				else
				{
					++iter;
				}
			}

			currentNode = NULL;
		}
	}


	//_currentPath.push_back(closestToMe);
	//_currentPath.push_back(closestToTarget);
}

Waypoint * h012164gPathfinder::GetNextWaypoint(Vector2D& myPosition, Vector2D& seekPosition)
{
	if (_currentPath.size() > 0)
	{
		bool collidedWithAnyWaypoint = false;
		bool collidedWithCorrectWaypoint = false;

		if (myTank->GetCentralPosition().Distance(_currentPath[_currentPath.size() - 1]->GetPosition()) < 25.0f)
		{
			_currentPath.pop_back();
		}

		if (_currentPath.size() == 0)
		{
			return nullptr;
		}
		else
		{
			return _currentPath[_currentPath.size() - 1];
		}

	}
	else
	{
		return nullptr;
	}
}

void h012164gPathfinder::SetTank(BaseTank * newTank)
{
	myTank = newTank;
}

void h012164gPathfinder::ResetPath()
{
	reachedEndOfPath = false;
}
