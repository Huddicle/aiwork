#include "h012164gTank.h"
#include "../TankManager.h"
#include "../Commons.h"
#include "../C2DMatrix.h"

//--------------------------------------------------------------------------------------------------

h012164gTank::h012164gTank(SDL_Renderer* renderer, TankSetupDetails details)
	: BaseTank(renderer, details) , targetPos(Vector2D(500.0, 800.0)), steering(h012164gSteering(mTanksICanSee))
{
	mTankTurnDirection  = DIRECTION_UNKNOWN;
	mTankTurnKeyDown	= false;
	mTankMoveDirection	= DIRECTION_NONE;
	mTankMoveKeyDown	= false;
	mManTurnDirection   = DIRECTION_UNKNOWN;
	mManKeyDown			= false;
	mFireKeyDown		= false;

	currentMaxSpeed = 25.0f;
	originalTankPosition = BaseTank::GetCentralPosition();
	behaviours = h012164gBEHAVIOURS::ARRIVE | h012164gBEHAVIOURS::OBSTACLEAVOIDANCE;

	steering.SetBehaviours(behaviours); //Bitwise operators etc. FLEE | SEEK | PURSUIT
	steering.SetSeekPosition(&targetPos);
	steering.SetMyTank(this);
	steering.GetPathfinder()->ResetPath();
	steering.GetPathfinder()->CreatePathToPosition(GetCentralPosition(), targetPos);
	steering.GetPathfinder()->SetTank(this);

	collectingPickup = false;
	prevPickupsSize = 0;
	currentNetForce = Vector2D(0.0, 0.0);
	runawayCooldownTimer = 500.0f;
	attackEnemy = true;
}

//--------------------------------------------------------------------------------------------------

h012164gTank::~h012164gTank()
{
}

//--------------------------------------------------------------------------------------------------

void h012164gTank::ChangeState(BASE_TANK_STATE newState)
{
	BaseTank::ChangeState(newState);
}

//--------------------------------------------------------------------------------------------------

void h012164gTank::Update(float deltaTime, SDL_Event e)
{

	switch (e.type)
	{
	case SDL_MOUSEBUTTONDOWN:
		//targetPos.x = e.button.x;
		//targetPos.y = e.button.y;
		//originalTankPosition = BaseTank::GetCentralPosition();
		//steering.GetPathfinder()->ResetPath();
		//steering.GetPathfinder()->CreatePathToPosition(GetCentralPosition(), targetPos);
		break;
	case SDL_KEYDOWN:
		//switch (e.key.keysym.sym)
		//{
		//case SDLK_1:
		//	behaviours = h012164gBEHAVIOURS::SEEK | h012164gBEHAVIOURS::OBSTACLEAVOIDANCE;
		//	steering.SetBehaviours(behaviours); //Bitwise operators etc. FLEE | SEEK | PURSUIT
		//	//cout << "SEEK" << endl;
		//	break;
		//case SDLK_2:
		//	behaviours = h012164gBEHAVIOURS::FLEE | h012164gBEHAVIOURS::OBSTACLEAVOIDANCE;
		//	steering.SetBehaviours(behaviours); //Bitwise operators etc. FLEE | SEEK | PURSUIT
		//	//cout << "FLEE" << endl;
		//	break;
		//case SDLK_3:
		//	behaviours = h012164gBEHAVIOURS::ARRIVE | h012164gBEHAVIOURS::OBSTACLEAVOIDANCE;
		//	steering.SetBehaviours(behaviours); //Bitwise operators etc. FLEE | SEEK | PURSUIT
		//	//cout << "ARRIVE" << endl;
		//	break;
		//case SDLK_4:
		//	behaviours = h012164gBEHAVIOURS::PURSUIT | h012164gBEHAVIOURS::OBSTACLEAVOIDANCE;
		//	steering.SetBehaviours(behaviours); //Bitwise operators etc. FLEE | SEEK | PURSUIT
		//	//cout << "PURSUIT" << endl;
		//	break;
		//}
		break;
	}

	if (mTanksICanSee.size() > 0)
	{
		if (GetHealth() < 50.0f)
		{
			behaviours = h012164gBEHAVIOURS::FLEE | h012164gBEHAVIOURS::OBSTACLEAVOIDANCE;
			steering.SetBehaviours(behaviours); //Bitwise operators etc. FLEE | SEEK | PURSUIT
			//cout << "FLEE" << endl;
			runawayCooldownTimer = 0.0f;
			ChangeState(BASE_TANK_STATE::TANKSTATE_DROPMINE);
		}
	}
	else
	{
		ChangeState(BASE_TANK_STATE::TANKSTATE_IDLE);
		if (runawayCooldownTimer > 2.5f)
		{
			behaviours = h012164gBEHAVIOURS::ARRIVE | h012164gBEHAVIOURS::OBSTACLEAVOIDANCE;
			steering.SetBehaviours(behaviours); //Bitwise operators etc. FLEE | SEEK | PURSUIT
			//cout << "ARRIVE" << endl;
		}
		runawayCooldownTimer += deltaTime;
	}

	vector<GameObject*> pickups = PickUpManager::Instance()->GetAllPickUps();
	if (!collectingPickup)
	{
		if (pickups.size() > 0)
		{
			int pickupID = 0;
			for (int i = 1; i < pickups.size(); i++)
			{
				if (GetCentralPosition().Distance(pickups[pickupID]->GetCentralPosition()) > GetCentralPosition().Distance(pickups[i]->GetCentralPosition()))
				{
					pickupID = i;
				}
			}

			targetPos = pickups[pickupID]->GetCentralPosition();
			originalTankPosition = BaseTank::GetCentralPosition();
			steering.GetPathfinder()->ResetPath();
			steering.GetPathfinder()->CreatePathToPosition(GetCentralPosition(), targetPos);
			collectingPickup = true;
		}
	}
	if (pickups.size() != prevPickupsSize)
	{
		collectingPickup = false;
		ChangeState(BASE_TANK_STATE::TANKSTATE_DROPMINE);
	}
	prevPickupsSize = pickups.size();

	Vector2D netForce = steering.GetNetForce(deltaTime);
	currentNetForce = netForce;
	//cout << netForce.x << ", " << netForce.y << endl;
	currentMaxSpeed = steering.GetPreferredMaxSpeed();
	//Determine the angle between the heading vector and the target.
	//double angle = acos(mHeading.Dot(netForce));


	float speedMultiplier = 1.0f;
	if (behaviours & h012164gBEHAVIOURS::ARRIVE)
	{
		Vector2D distanceVector = targetPos - originalTankPosition;
		Vector2D distanceLeftVector = targetPos - BaseTank::GetCentralPosition();

		speedMultiplier = distanceLeftVector.Length() / 250.0f;
		if (speedMultiplier > 1.0f) speedMultiplier = 1.0f;

	}

	h012164gRotateHeadingByRadian(2.5f, mHeading.Sign(netForce), deltaTime);

	mCurrentSpeed = currentMaxSpeed * speedMultiplier;
	//std::cout << GetMaxSpeed() * speedMultiplier << std::endl;

	if (mCurrentSpeed > GetMaxSpeed())
	{
		mCurrentSpeed = GetMaxSpeed();
	}
	//BaseTank::RotateHeadingToFacePosition(GetCentralPosition() + mVelocity, deltaTime);

	if (mTanksICanSee.size() > 0)
	{
		ChangeState(TANKSTATE_MANFIRE);
		RotateManByRadian(kManTurnRate * 2.0f, mManFireDirection.Sign(GetCentralPosition() - (mTanksICanSee[0]->GetCentralPosition() + (mTanksICanSee[0]->GetVelocity()))), deltaTime);
	}



	//Call parent update.
	BaseTank::Update(deltaTime, e);
}

void h012164gTank::Render()
{
#ifdef AUDIO_VISIBLE
	DrawDebugCircle(targetPos, 4.0f, 255, 0, 255);
#endif
	
	vector<Vector2D> feelers = steering.GetFeelers();

	for (int i = 0; i < feelers.size(); i++)
	{
		DrawDebugLine(GetCentralPosition(), GetCentralPosition() + (feelers[i]), 0, 0, 255);
		DrawDebugCircle(GetCentralPosition() + (feelers[i]), 5.0f, 0, 0, 255);
	}
	DrawDebugLine(GetCentralPosition(), GetCentralPosition() - currentNetForce, 255, 255, 0); 

	vector<Waypoint*> waypoints = steering.GetPathfinder()->GetCurrentPath();
	for (int i = 0; i < waypoints.size(); i++)
	{
		DrawDebugCircle(waypoints[i]->GetPosition(), 10.0f, 255, 20, 147);
	}
	if (waypoints.size() > 0)
	{
		DrawDebugCircle(waypoints[waypoints.size() - 1]->GetPosition(), 10.0f, 255, 147, 20);
	}

	BaseTank::Render();
}

void h012164gTank::SetCurrentMaxSpeed(float newMaxSpeed)
{
	if (newMaxSpeed > mMaxSpeed)
	{

	}
	else
	{
		currentMaxSpeed = newMaxSpeed;
	}
}

//--------------------------------------------------------------------------------------------------

bool h012164gTank::IsSameDirection(const Vector2D & vec1, const Vector2D & vec2)
{
	if (abs(abs(vec1.x) - abs(vec2.x)) > 0.04)
	{
		return false;
	}
	if (abs(abs(vec1.y) - abs(vec2.y)) > 0.04)
	{
		return false;
	}
	return true;
}

bool h012164gTank::IsSoftTurn(const Vector2D & vec1, const Vector2D & vec2)
{
	float dot = vec1.Dot(vec2);

	float theta = acosf(dot / (vec1.Length() * vec2.Length()));
	theta -= 3.1415926f;
	theta = abs(theta);

	if (theta > 0.5f)
	{
		return false;
	}
	else
	{
		return true;
	}
}

bool h012164gTank::IsHardTurn(const Vector2D & vec1, const Vector2D & vec2)
{
	if (abs(abs(vec1.x) - abs(vec2.x)) > 1.0f)
	{
		return false;
	}
	if (abs(abs(vec1.y) - abs(vec2.y)) > 1.0f)
	{
		return false;
	}
	return true;
}

void h012164gTank::MoveInHeadingDirection(float deltaTime)
{
	//Get the force that propels in current heading.
	Vector2D force = (mHeading*mCurrentSpeed)-mVelocity;

	//Acceleration = Force/Mass
	Vector2D acceleration = force/GetMass();

	//Update velocity.
	mVelocity += acceleration * deltaTime;
	
	//Don't allow the tank does not go faster than max speed.
	mVelocity.Truncate(GetMaxSpeed()); //TODO: Add Penalty for going faster than MAX Speed.

	//Finally, update the position.
	Vector2D newPosition = GetPosition();
		newPosition.x += mVelocity.x*deltaTime;
		newPosition.y += (mVelocity.y/**-1.0f*/)*deltaTime;	//Y flipped as adding to Y moves down screen.
	SetPosition(newPosition);
}

void h012164gTank::h012164gRotateHeadingByRadian(float radian, int sign, float deltaTime)
{
	//Clamp the amount to turn to the max turn rate.
	if (radian > mMaxTurnRate)
		radian = mMaxTurnRate;
	else if (radian < -mMaxTurnRate)
		radian = -mMaxTurnRate;
	//IncrementTankRotationAngle(RadsToDegs(radian));
	mRotationAngle += RadsToDegs(radian)*sign * deltaTime;

	//Usee a rotation matrix to rotate the player's heading
	C2DMatrix RotationMatrix;

	//Calculate the direction of rotation.
	RotationMatrix.Rotate(radian * sign * deltaTime);
	//Get the new heading.
	RotationMatrix.TransformVector2Ds(mHeading);

	//cout << "RotateHeadingByRadian -- Heading x = " << mHeading.x << " y = " << mHeading.y << endl;

	//Get the new velocity.
	RotationMatrix.TransformVector2Ds(mVelocity);

	//Side vector must always be perpendicular to the heading.
	mSide = mHeading.Perp();
}

//---------------------------------------------------------------------------------------------------